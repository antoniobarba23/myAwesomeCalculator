const container = document.getElementById('container')

container.style = 'background-color: #eee; padding: 4em'

const calculatorButtons = [7, 8, 9, '/', 4, 5, 6, '*', 1, 2, 3, '-', 0, 'C', '=', '+']
const buttonsArray = []

let firstValue = ''
let secondValue = ''
let operator = ''
let activeOperator = null
let selectedOperator = ''
let nonActiveOperator = ''

const buttonStyle = 'font-size: 16px; padding: 2em; margin-right: 10px; margin-top: 10px; background-color: white; border-radius: 5px'
const hoverStyle = 'cursor:pointer; font-size: 16px; padding: 2em; margin-right: 10px; margin-top: 10px; background-color: tian; border-radius: 5px'
const activeStyle = 'font-size: 16px; padding: 2em; margin-right: 10px; margin-top: 10px; background-color: #ABDBE3; border-radius: 5px'

const calculate = () => {
    const input = document.getElementById('result')
    switch (operator) {
        case '+':
            input.value = parseInt(firstValue) + parseInt(secondValue)
            break;
        case '-':
            input.value = parseInt(firstValue) - parseInt(secondValue)
            break;
        case '*':
            input.value = parseInt(firstValue) * parseInt(secondValue)
            break;
        case '/':
            input.value = parseInt(firstValue) / parseInt(secondValue)
            break;
    }
}

const setValuesToCalculate = (val) => {
    const input = document.getElementById('result')
    const button = document.getElementById('btn' + val)

    if (Number.isInteger(val)) {
        if (operator) {
            secondValue += val.toString()
            input.value = secondValue
        } else {
            firstValue += val.toString()
            input.value = firstValue
        }
    } else {
        switch (val) {
            case '=':
                selectedOperator = document.getElementById('btn' + activeOperator)
                selectedOperator.style = buttonStyle
                //activeOperator = null
                calculate()
                operator = null
                firstValue = input.value
                secondValue = ''
                break
            case 'C':
                selectedOperator = document.getElementById('btn' + activeOperator)
                selectedOperator.style = buttonStyle
                firstValue = ''
                secondValue = ''
                operator = ''
                input.value= ''
                break;
            default:
                if(firstValue)  {
                    button.style = activeStyle
                    activeOperator = val
                    operator = val
                    input.value = '0'
                }
        }
    }
    console.log('First element ', firstValue)
    console.log('Second element ', secondValue)
    console.log('Operator ', operator)
}

document.addEventListener('keyup', (event) => {
    //console.log('event', event)
    const presedEvent = calculatorButtons.find(el => el.toString() === event.key)
        if (presedEvent !== undefined) setValuesToCalculate(presedEvent)
        if(event.key === 'Enter' ) setValuesToCalculate('=')
        if(event.key === 'Backspace' ) setValuesToCalculate('C')
        if(event.key === 'Escape' ) setValuesToCalculate('C')
})

calculatorButtons.forEach((el, index) => {
    const button = document.createElement('button')
    button.textContent = el
    button.style = buttonStyle
    button.addEventListener('mouseenter', () => activeOperator !== el ? button.style = hoverStyle: null)
    button.addEventListener('mouseleave', () => activeOperator !== el ? button.style = buttonStyle: null)
    //button.addEventListener('mouseleave', () => button.style = 'font-size: 16px; padding: 2em; margin-right: 10px; margin-top: 10px; background-color: white; border-radius: 5px')
    //button.addEventListener('click', () => button.style = 'font-size: 16px; padding: 2em; margin-right: 10px; margin-top: 10px; background-color: gray; border-radius: 5px')
    button.addEventListener('click', () => setValuesToCalculate(el))
    button.id = 'btn' + el
    buttonsArray.push(button)

    if ((index + 1) % 4 === 0) {0
        const br = document.createElement('br')
        buttonsArray.push(br)
    }
})

const result = document.createElement('input')
result.placeholder = 'Resultado'
result.style = 'margin-top: 2em; width: 310px; padding: 15px; border-radius: 5px'
result.id = 'result'
buttonsArray.push(result)

buttonsArray.forEach(btn => container.append(btn))

//Actividad
/* Tomar la calculadora actual, despues de que se agregue un resultado o que se calcuyle
si yo presiono otro operador debe tomar el valor resultado anterior como el
valor inicial de calulo para una segunda operacion, ingreso el segundo valor y
me recalcula ✔✔

solucionar el problema que tiene la calculadora cuando no hay ningun valor seleccionado
y presiono un operador, no debe de permitirlo, que el operador no ingrese ✔✔

si se seleccionan multiples operadores que se quite el anterior y se ponga el nuevo  
*/
